/**
 * Food orders backend
 * Food orders platform
 *
 * OpenAPI spec version: 0.0.1
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs/Observable';

import { Setting } from '../model/setting';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';
import {ChangeSettingCommand} from "../model/changeSettingCommand";


@Injectable()
export class SettingControllerService {

    protected basePath = 'http://localhost:8080';//https://calm-dusk-20347.herokuapp.com';
   // protected basePath = 'https://calm-dusk-20347.herokuapp.com';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (let consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * getSettings
     *
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getSettingsUsingGET(observe?: 'body', reportProgress?: boolean): Observable<Array<Setting>>;
    public getSettingsUsingGET(observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<Setting>>>;
    public getSettingsUsingGET(observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<Setting>>>;
    public getSettingsUsingGET(observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
            'application/json'
        ];

        return this.httpClient.get<Array<Setting>>(`${this.basePath}/api/settings`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }


  /**
   * changeSetting
   *
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  /**
   * changeSetting
   *
   * @param changeSettingCommand changeSettingCommand
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public changeSettingUsingPOST(changeSettingCommand: ChangeSettingCommand, observe?: 'body', reportProgress?: boolean): Observable<any>;
  public changeSettingUsingPOST(changeSettingCommand: ChangeSettingCommand, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
  public changeSettingUsingPOST(changeSettingCommand: ChangeSettingCommand, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
  public changeSettingUsingPOST(changeSettingCommand: ChangeSettingCommand, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
    if (changeSettingCommand === null || changeSettingCommand === undefined) {
      throw new Error('Required parameter changeSettingCommand was null or undefined when calling changeSettingUsingPOST.');
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = [
      '*/*'
    ];
    let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
    if (httpHeaderAcceptSelected != undefined) {
      headers = headers.set("Accept", httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    let consumes: string[] = [
      'application/json'
    ];
    let httpContentTypeSelected:string | undefined = this.configuration.selectHeaderContentType(consumes);
    if (httpContentTypeSelected != undefined) {
      headers = headers.set("Content-Type", httpContentTypeSelected);
    }

    return this.httpClient.post<any>(`${this.basePath}/api/settings`,
      changeSettingCommand,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

  /**
   * getSettingByKey
   *
   * @param key key
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public getSettingByKeyUsingGET(key: string, observe?: 'body', reportProgress?: boolean): Observable<Setting>;
  public getSettingByKeyUsingGET(key: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Setting>>;
  public getSettingByKeyUsingGET(key: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Setting>>;
  public getSettingByKeyUsingGET(key: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
    if (key === null || key === undefined) {
      throw new Error('Required parameter key was null or undefined when calling getSettingByKeyUsingGET.');
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = [
      '*/*'
    ];
    let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
    if (httpHeaderAcceptSelected != undefined) {
      headers = headers.set("Accept", httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    let consumes: string[] = [
      'application/json'
    ];

    return this.httpClient.get<Setting>(`${this.basePath}/api/settings/${encodeURIComponent(String(key))}`,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

}
