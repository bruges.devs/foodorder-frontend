export * from './categoryController.service';
import {CategoryControllerService} from './categoryController.service';
import {OrderControllerService} from './orderController.service';
import {ProductControllerService} from './productController.service';

export * from './orderController.service';
export * from './productController.service';
export const APIS = [CategoryControllerService, OrderControllerService, ProductControllerService];
