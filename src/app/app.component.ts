import {Component} from '@angular/core';
import {Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {TabsPage} from '../pages/tabs/tabs';
import {DatabaseService} from "../core/shared/service/database.service";
import {LoggingService} from "../core/shared/service/logging.service";
import {TranslateService} from "@ngx-translate/core";
import {DatabaseUpdatesService} from "../core/shared/service/database-updates.service";
import {FirstStartupService} from "../core/shared/service/first-startup.service";
import {NetworkService} from "../core/shared/service/network.service";
import {AppCreator} from "../core/shared/app/app-creator";
import {AppCreatorListener} from "../core/shared/app/app-creator-listener";
import {ProductFacade} from "../pages/facade/product.facade";
import {LogLevel} from "../core/shared/constants/loglevel";
import {ShopsFacade} from "../pages/facade/shops.facade";
import {ShopsPage} from "../pages/shops/shops";
import {ShopsFavoritePage} from "../pages/shops-favorite/shops-favorite";
import {SettingFactory} from "../factory/setting.factory";
import {SettingFacade} from "../pages/facade/setting.facade";
import {Setting} from "../providers/model/setting";
import {_catch} from "rxjs/operator/catch";

@Component({
  templateUrl: 'app.html'
})
export class MyApp implements AppCreatorListener {

  rootPage: any = TabsPage;

  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              private translate: TranslateService,
              private databaseUpdatesService: DatabaseUpdatesService,
              private firstStartupService: FirstStartupService,
              private logger: LoggingService,
              private databaseService: DatabaseService<any>,
              private networkService: NetworkService,
              private productFacade: ProductFacade,
              private shopsFacade: ShopsFacade,
              private settingsFacade: SettingFacade,
              private appCreator: AppCreator) {
    this.initTranslate();

    this.appCreator.appCreatorListener = this;
    this.determineStartScreen();

    platform.ready().then(() => {
      this.appCreator.prepareApp().then(value => {
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        console.log("Platforms: "+ platform.platforms());
        if (!platform.is('mobileweb')){
          this.initOneSignal();
        }
        statusBar.styleDefault();
        splashScreen.hide();
      });
    });

    platform.pause.subscribe(() => {
      this.appCreator.stopOrderChangeListening();
    });
  }

  initOneSignal() {
    // OneSignal Code start:
    // Enable to debug issues:
    //window["plugins"].OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});//TODO LOGGING

    var notificationOpenedCallback = function (jsonData) {
      this.logger.log(LogLevel.DEBUG, 'notificationOpenedCallback: ', JSON.stringify(jsonData));
    };

    window["plugins"].OneSignal
      .startInit("629ea531-45a1-4281-88ec-327e89dac505", "862966117955")
      .handleNotificationOpened(notificationOpenedCallback)
      .endInit();

    window["plugins"].OneSignal.getIds((ids) => {
      this.logger.log(LogLevel.FINE, "One Signal ids: ", ids);
      window.localStorage.setItem('ONE_SIGNAL_USER_ID', ids.userId);
    });
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('nl');
    this.translate.use('nl'); // Set your language here
  }

  initializeAppAfterErrors() {
    //TODO
  }

  appIsPreparedHandler(): Promise<boolean> {
   return this.settingsFacade.loadSettingByKey('CAN_APPROVE')
     .then((hasFetchedSetting: boolean)=>true);
  }

  private determineStartScreen() {
    if (!this.shopsFacade.hasChosenShop()) {
      this.rootPage = ShopsFavoritePage;
      this.shopsFacade.loadShops();//TODO FIND APPROPRIATE PLACE
    }
  }
}
