import {ErrorHandler, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {MyApp} from './app.component';

import {BasketPage} from '../pages/basket/basket';
import {OrdersPage} from '../pages/orders/orders';
import {ProductPage} from '../pages/product/product';
import {TabsPage} from '../pages/tabs/tabs';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {ProductFacade} from "../pages/facade/product.facade";
import {ApiModule} from "../providers";
import {ProductService} from "../core/service/product.service";
import {HttpClientModule} from "@angular/common/http";
import {Products} from "../components/products/products";
import {BasketService} from "../core/service/basket.service";
import {BasketDao} from "../core/dao/basket.dao";
import {I18nModule} from "../core/shared/i18n/i18n.module";
import {BasketFacade} from "../pages/facade/basket.facade";
import {ProductDao} from "../core/dao/product.dao";
import {OrderService} from "../core/service/order.service";
import {SharedModule} from "../core/shared/shared-module";
import {OrderDialogComponent} from "../components/order-dialog/order-dialog";
import {OrderDao} from "../core/dao/order.dao";
import {OrderFacade} from "../pages/facade/order.facade";
import {CategoryService} from "../core/service/category.service";
import {ShopsFacade} from "../pages/facade/shops.facade";
import {ShopService} from "../core/service/shop.service";
import {ShopsPage} from "../pages/shops/shops";
import {ShopsFavoritePage} from "../pages/shops-favorite/shops-favorite";
import {SettingsPage} from "../pages/settings/settings";
import {SettingService} from "../core/service/setting.service";
import {SettingFacade} from "../pages/facade/setting.facade";

@NgModule({
  declarations: [
    MyApp,
    BasketPage,
    OrdersPage,
    ProductPage,
    TabsPage,
    OrderDialogComponent,
    Products,
    ShopsPage,
    ShopsFavoritePage,
    SettingsPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    I18nModule,
    ApiModule,
    SharedModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    BasketPage,
    OrdersPage,
    ProductPage,
    OrderDialogComponent,
    TabsPage,
    ShopsPage,
    ShopsFavoritePage,
    SettingsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    OrderFacade,
    OrderService,
    OrderDao,
    ProductDao,
    ProductFacade,
    ProductService,
    BasketFacade,
    BasketService,
    BasketDao,
    CategoryService,
    ShopsFacade,
    ShopService,
    SettingFacade,
    SettingService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {
}
