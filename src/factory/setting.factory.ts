import {SettingText} from "../core/model/setting/setting-text";
import {SettingModel} from "../core/model/setting/setting.model";
import {SettingBoolean} from "../core/model/setting/setting-boolean";
import {SettingNumber} from "../core/model/setting/setting-number";
import {SettingList} from "../core/model/setting/setting-list";

export class SettingFactory {
  static createSettingBasedOnType(type: string, value: any): SettingModel {
    switch (type) {
      case "TEXT":
        return new SettingText(value);
      case "BOOLEAN":
        return new SettingBoolean(value);
      case "NUMBER":
        return new SettingNumber(value);
      case "LIST":
        return new SettingList(value);
      default:
        return new SettingText(value);

    }
  }
}
