import {Component} from '@angular/core';
import {NavParams, ViewController} from "ionic-angular";
import {OrderModel} from "../../core/model/order.model";
import {LoggingService} from "../../core/shared/service/logging.service";
import {LogLevel} from "../../core/shared/constants/loglevel";
import {BasketFacade} from "../../pages/facade/basket.facade";
import {MyToastService} from "../../core/shared/service/my-toast.service";


/**
 * Generated class for the OrderDialogComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'order-dialog',
  templateUrl: 'order-dialog.html'
})
export class OrderDialogComponent {

  order: OrderModel;

  constructor(private view: ViewController,
              private logger: LoggingService,
              private navParams: NavParams,
              private facade: BasketFacade,
              private toastCtrl: MyToastService) {
    this.order = this.navParams.get('order');
  }

  closeModal() {
    this.view.dismiss();
  }

  doOrder() {
    if (!this.order.dateWanted) {
      this.toastCtrl.showToast("Gelieve een gewenste afhaaltijd op te geven", 2000);
      return;
    }
    if (!this.order.firstName || this.order.firstName.trim() === "") {
      this.toastCtrl.showToast("Gelieve een voornaam op te geven", 2000);
      return;
    }
    if (!this.order.lastName || this.order.lastName.trim() === "") {
      this.toastCtrl.showToast("Gelieve een familienaam op te geven", 2000);
      return;
    }

    if (this.order.dateWanted.getTime() > Date.now()) {
      this.logger.log(LogLevel.DEBUG, "Start procedure for order: ", this.order);
      this.facade.placeOrder(this.order);
      this.view.dismiss(this.order);
    } else {
      this.toastCtrl.showToast("Gelieve een afhaaltijd in de toekomst te geven", 2000);
    }
  }
}
