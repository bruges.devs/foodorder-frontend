import {NgModule} from '@angular/core';
import {OrderDialogComponent} from './order-dialog/order-dialog';
import {Products} from "./products/products";

@NgModule({
	declarations: [OrderDialogComponent, Products],
	imports: [],
	exports: [OrderDialogComponent, Products]
})
export class ComponentsModule {}
