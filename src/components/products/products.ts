import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ProductFacade} from "../../pages/facade/product.facade";
import {Events, Refresher} from "ionic-angular";
import {EventChannels} from "../../core/constants/event-channels";
import {CategoryModel} from "../../core/model/category.model";
import {ProductModel} from "../../core/model/product.model";
import {ShopModel} from "../../core/model/shop.model";

/**
 * Generated class for the Products component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'products',
  templateUrl: 'products.html'
})
export class Products implements OnInit, OnDestroy{

  private refresher: Refresher;

  constructor(protected facade: ProductFacade,
              private changeRef: ChangeDetectorRef,
              private events: Events) {
    this.events.subscribe(EventChannels.PRODUCTS_LOADED, ()=> this.refreshEnded());
    this.events.subscribe(EventChannels.PRODUCTS_LOADED_FAILED, ()=> this.refreshEnded());
    this.events.subscribe(EventChannels.CATEGORIES_FILTER,()=> this.changeRef.detectChanges());
  }

  ngOnDestroy(): void {
  }

  ngOnInit(): void {
    this.facade.loadShop(new ShopModel(JSON.parse(window.localStorage.getItem('shop'))));
  }

  addToBasket(product: ProductModel) {
    this.facade.addToBasket(product);
  }

  emptyBasket(product: ProductModel){
    this.facade.emptyBasket(product);
  }

  doRefresh(refresher: Refresher) {
    this.refresher = refresher;
    this.facade.loadProducts();
  }

  private refreshEnded() {
    if (this.refresher){
      this.refresher.complete();
    }
  }

  filterOnCategory(category: CategoryModel) {
    this.facade.filterOnCategory(category);
  }
}
