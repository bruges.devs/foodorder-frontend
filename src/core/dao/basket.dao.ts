import {DatabaseService, DatabaseStatement} from "../shared/service/database.service";
import {Injectable} from "@angular/core";
import {Product} from "../../providers";
import {BasketItem} from "../model/basket-item";
import {MapperBasketItem} from "../mapper/db/mapper.basket-item";
import {MapperBasket} from "../mapper/db/mapper.basket";
import {LoggingService} from "../shared/service/logging.service";
import {LogLevel} from "../shared/constants/loglevel";
import {ProductModel} from "../model/product.model";

@Injectable()
export class BasketDao {

  constructor(private databaseService: DatabaseService<BasketItem>, private logger: LoggingService) {
  }

  emptyBasket(product: ProductModel): Promise<boolean> {
    let statement = new DatabaseStatement("DELETE FROM tbl_basket WHERE product_id = ? AND is_ordered = ?",
      [product.id, 0]);

    return this.databaseService.executeSql(statement)
      .then((isRemoved: boolean) => {
        this.logger.log(LogLevel.FINE, "removeProductFromBasket: ", isRemoved);
        return isRemoved;
      });
  }

  addInBasket(product: ProductModel): Promise<boolean> {
    let statement = new DatabaseStatement("INSERT INTO tbl_basket (product_id, amount, is_ordered) VALUES (?,?,?)",
      [product.id, 1, 0]);

    return this.databaseService.executeSql(statement)
      .then((isAdded: boolean) => {
        this.logger.log(LogLevel.FINE, "addInBasket: ", isAdded);
        return isAdded;
      });
  }

  updateBasket(basketItem: BasketItem): Promise<boolean> {
    let statement = new DatabaseStatement("UPDATE tbl_basket SET amount= ?, is_ordered = ?" +
      " WHERE product_id = ? AND is_ordered = ?",
      [basketItem.amount, basketItem.ordered, basketItem.product.id, 0]);

    return this.databaseService.executeSql(statement)
      .then((isUpdated: boolean) => {
        this.logger.log(LogLevel.FINE, "updateBasket: ", isUpdated);
        return isUpdated;
      })
  }

  getBasketItemByProductId(product: ProductModel): Promise<BasketItem> {
    let mapper = new MapperBasketItem();
    let statement = new DatabaseStatement("SELECT * FROM tbl_basket JOIN tbl_product ON tbl_product.id = tbl_basket.product_id" +
      " WHERE tbl_product.id = ? AND is_ordered = ?",
      [product.id, 0]);

    return this.databaseService.getSingleResult(statement, mapper)
      .then((basketItem: BasketItem) => {
        this.logger.log(LogLevel.FINE, "getBasketItemByProductId: ", basketItem);
        return basketItem;
      });
  }

  updateBasketCount(): Promise<number> {
    let mapper = new MapperBasketItem();
    let statement = new DatabaseStatement("SELECT * FROM tbl_basket where is_ordered = ?", [0]);

    return this.databaseService.getResultList(statement, mapper)
      .then(items => {
        this.logger.log(LogLevel.FINE, "updateBasketCount: ", items.length);
        return items.length;
      });
  }

  loadBasket(): Promise<BasketItem[]> {
    let mapper = new MapperBasket();
    let statement = new DatabaseStatement("SELECT * FROM tbl_basket JOIN tbl_product ON tbl_product.id = tbl_basket.product_id " +
      "WHERE tbl_basket.is_ordered = ?", [0]);

    return this.databaseService.getResultList(statement, mapper)
      .then((basketItems: BasketItem[]) => {
        this.logger.log(LogLevel.FINE, "loadBasket: ", basketItems);
        return basketItems;
      });
  }

  getBasketItemsByOrderId(orderId: number): Promise<BasketItem[]> {
    let statement = new DatabaseStatement("SELECT * FROM tbl_basket JOIN tbl_product ON tbl_product.id = tbl_basket.product_id "+
      "WHERE tbl_basket.is_ordered = ? AND order_id = ?",
      [1, orderId]);
    return this.databaseService.getResultList(statement, new MapperBasket());
  }

  getBasketItems(): Promise<BasketItem[]> {
    let statement = new DatabaseStatement("SELECT * FROM tbl_basket WHERE tbl_basket.is_ordered = 0", []);
    return this.databaseService.getResultList(statement, new MapperBasket());
  }

}
