import {Injectable} from "@angular/core";
import {OrderModel} from "../model/order.model";
import {DatabaseService, DatabaseStatement} from "../shared/service/database.service";
import {LoggingService} from "../shared/service/logging.service";
import {MapperOrderLazy} from "../mapper/db/mapper.order.lazy";
import {BasketItem} from "../model/basket-item";
import {LogLevel} from "../shared/constants/loglevel";
import {StringUtils} from "../shared/util/string-utils";

@Injectable()
export class OrderDao {

  constructor(private databaseService: DatabaseService<OrderModel>, private logger: LoggingService) {
  }

  getOrderById(id: number): Promise<OrderModel> {
    let statement = new DatabaseStatement("SELECT * FROM tbl_order WHERE id = ?", [id]);
    return this.databaseService.getSingleResult(statement, new MapperOrderLazy());
  }

  loadOrders(): Promise<OrderModel[]> {
    // let statement = new DatabaseStatement("SELECT o.id as OrderId, b.id as basketId, p.id as productId* FROM tbl_order o JOIN tbl_basket b ON b.order_id = o.id " +
    //   "JOIN tbl_product p ON p.id = b.product_id", []);
    let statement = new DatabaseStatement("SELECT * FROM tbl_order", []);
    return this.databaseService.getResultList(statement, new MapperOrderLazy());
  }

  insertOrder(order: OrderModel): Promise<OrderModel> {
    let statement = new DatabaseStatement("INSERT INTO tbl_order (id,description, price, date_placed, date_wanted, status, first_name, last_name) VALUES (?,?,?,?,?,?,?,?)"
      , order.getParams());
    return this.databaseService.insertEntity(order, statement, "id");
  }

  updateOrder(order: OrderModel): Promise<boolean> {
    let statement = new DatabaseStatement("UPDATE tbl_order SET description=?, price=?, date_placed=?, date_wanted=?, status=? WHERE id = ?"
      , order.getParams().concat(order.id));
    return this.databaseService.executeSql(statement);
  }

  updateOrderItems(orderItems: BasketItem[], orderId: number) {
    let statement = new DatabaseStatement("UPDATE tbl_basket SET order_id = ?, is_ordered = ? WHERE is_ordered = ? AND product_id IN (" +
      StringUtils.join(orderItems.map(orderItem => '' + orderItem.product.id)) + ")", [orderId, 1, 0]);

    return this.databaseService.executeSql(statement)
      .then((isUpdated: boolean) => {
        this.logger.log(LogLevel.FINE, "updateBasket order_id: ", isUpdated);
        return isUpdated;
      });
  }

  //TODO WARNING: don't delete the items just on product id
  deleteOrderItems(orderItems: BasketItem[]) {
    let statement = new DatabaseStatement("DELETE FROM tbl_basket WHERE product_id IN (" +
      StringUtils.join(orderItems.map(orderItem => '' + orderItem.product.id)) + ")",
      []);

    return this.databaseService.executeSql(statement)
      .then((isUpdated: boolean) => {
        this.logger.log(LogLevel.FINE, "Deleted basket-items: ", isUpdated);
        return isUpdated;
      });
  }
}
