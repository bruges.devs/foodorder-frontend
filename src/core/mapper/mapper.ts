
export interface Mapper<F, T> {
  mapTo(item: F): T;
}
