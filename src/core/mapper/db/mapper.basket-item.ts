import {RowMapper} from "../../shared/mapper/rowmapper";
import {BasketItem} from "../../model/basket-item";
import {RowMapperProduct} from "./row-mapper.product";

export class MapperBasketItem implements RowMapper<BasketItem> {

  mapRow(from: any, index: number): BasketItem {
    let result = new BasketItem();
    result.product = new RowMapperProduct().mapRow(from,index);
    result.amount = from[index].amount;
    return result;
  }

}
