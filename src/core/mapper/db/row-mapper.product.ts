import {RowMapper} from "../../shared/mapper/rowmapper";
import {ProductModel} from "../../model/product.model";
import {MapperCategory} from "../api/mapper.category";

export class RowMapperProduct implements RowMapper<ProductModel> {
  mapRow(from: any, index: number): ProductModel {
    let product: ProductModel = new ProductModel();
    product.id = from.product_id;
    product.name = from.name;
    product.category = new MapperCategory().mapTo(from);
    product.description = from.description ? from.description : '';
    product.price = from.price;
    return product;
  }

}
