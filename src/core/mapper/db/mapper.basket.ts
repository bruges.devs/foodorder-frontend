import {RowMapper} from "../../shared/mapper/rowmapper";
import {BasketItem} from "../../model/basket-item";
import {RowMapperProduct} from "./row-mapper.product";
import {RowsMapper} from "../../shared/mapper/rows.mapper";

export class MapperBasket implements RowMapper<BasketItem>, RowsMapper<BasketItem> {

  mapRow(from: any, index: number): BasketItem {

    let result = new BasketItem();
    result.amount = from[index].amount;
    result.ordered = from[index].is_ordered;
    result.orderId = from[index].order_id;
    result.product = new RowMapperProduct().mapRow(from[index], index);
    return result;
  }

  mapList(from: any, index: number): BasketItem[] {
    let resultSet = [];
    for (let i = 0; i < from.length; i++) {
      resultSet.push(this.mapRow(from, i));
    }
    return resultSet;
  }

}
