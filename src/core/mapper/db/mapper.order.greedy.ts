import {RowMapper} from "../../shared/mapper/rowmapper";
import {MapperBasket} from "./mapper.basket";
import {OrderModel} from "../../model/order.model";
import {OrderStatus} from "../../model/order-status";
import moment = require("moment");

export class MapperOrderGreedy implements RowMapper<OrderModel> {
  mapRow(from: any, index: number): OrderModel {
    let order = new OrderModel();
    order.id = from[index].id;
    order.description = from[index].description;
    order.price = from[index].price;
    order.datePlaced = moment(from[index].date_placed, OrderModel.DATE_TIME_FORMAT).toDate();
    order.dateWanted = moment(from[index].date_wanted, OrderModel.DATE_TIME_FORMAT).toDate();
    order.status = new OrderStatus(from[index].status);
    order.orderItems = new MapperBasket().mapList(from, index);
    return order;
  }

}
