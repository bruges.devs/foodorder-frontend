
export interface ListMapper<F, T> {
  mapList(items: F[]): T[];
}
