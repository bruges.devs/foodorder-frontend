import {OrderItem} from "../../../providers/index";
import {BasketItem} from "../../model/basket-item";

export class MapperBasketItemToOrderItem {

  mapList(basketItems: BasketItem[]): OrderItem[] {
    let orderItem: any;
    let productId: number;
    let orderItems: OrderItem[] = [];
    for (let i = 0; i < basketItems.length; i++) {
      productId =  basketItems[i].product.id;
      orderItem = {
        productId: productId,
        quantity: basketItems[i].amount
      };
      orderItems.push(orderItem);
    }
    return orderItems;
  }
}
