import {CategoryModel} from "../../model/category.model";
import {Mapper} from "../mapper";
import {ListMapper} from "../list-mapper";
import {Category} from "../../../providers";


export class MapperCategory implements Mapper<Category, CategoryModel>, ListMapper<Category, CategoryModel> {

  mapTo(from: any): CategoryModel {
    let category = new CategoryModel();
    category.id = from.id;
    category.name = from.name;
    return category;
  }

  mapList(from: any): CategoryModel[] {
    let categories = [];
    for (let i = 0; i < from.length; i++) {
      categories.push(this.mapTo(from[i]));
    }
    return categories;
  }

}
