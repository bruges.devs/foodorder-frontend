import {ProductModel} from "../../model/product.model";
import {Product} from "../../../providers";
import {ListMapper} from "../list-mapper";
import {Mapper} from "../mapper";
import {MapperCategory} from "./mapper.category";

export class MapperProduct implements Mapper<Product, ProductModel>, ListMapper<Product, ProductModel> {

  mapList(items: Product[]): ProductModel[] {
    let list: ProductModel[] = [];
    for (let i = 0; i < items.length; i++) {
      list.push(this.mapTo(items[i]));
    }
    return list;
  }

  mapTo(item: Product): ProductModel {
    let product: ProductModel = new ProductModel();
    product.id = item.id;
    product.name = item.name;
    product.description = item.description;
    if (item.category) {
      product.category = new MapperCategory().mapTo(item.category);
    }
    product.price = item.price;
    return product;
  }

}
