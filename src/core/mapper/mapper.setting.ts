import {SettingFactory} from "../../factory/setting.factory";
import {SettingModel} from "../model/setting/setting.model";

export class MapperSetting {

   mapList(from: any): SettingModel[] {
    let settingList: SettingModel[] = [];
    for (let i = 0; i < from.length; i++) {
      settingList.push(this.mapRow(from[i]));
    }
    return settingList;
  }

  mapRow(from: any): SettingModel {
    let setting = SettingFactory.createSettingBasedOnType(from.type, from.value);
    setting.key = from.key;
    setting.id = from.id;
    return setting;
  }

}
