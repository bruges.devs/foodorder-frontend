export class BasketItemDto {
  productId: string;
  amount: number;
}
