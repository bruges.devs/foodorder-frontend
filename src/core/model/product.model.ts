import {CategoryModel} from "./category.model";

export class ProductModel {

  category?: CategoryModel;
  description?: string;
  id?: number;
  name?: string;
  price?: number;

  private _isSaved: boolean = true;

  isActive: boolean = false;

  get isSaved(): boolean {
    return this._isSaved;
  }

  constructor() {
  }

  toggleProductContainer() {
    this.isActive = !this.isActive;
  }

  productChanged() {
    this._isSaved = false;
  }

  getParams() {
    return [this.id, this.category ? this.category.id : -1, this.category ? this.category.name : "", this.name, this.description ? this.description : '', this.price];
  }
}
