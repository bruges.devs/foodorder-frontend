import {BasketItem} from "./basket-item";
import {OrderStatus} from "./order-status";
import moment = require("moment");

export class OrderModel {

  static TIME_FORMAT: string = "HH:mm";
  static DATE_TIME_FORMAT: string = "DD/MM/YYYY HH:mm";

  private _dateWanted: Date;

  id: number;
  description: string;
  price: string;
  firstName: string;
  lastName: string;
  datePlaced: Date;
  status: OrderStatus;
  orderItems: BasketItem[];

  constructor() {
    console.log("DATE: " + new Date(Date.now()));
    console.log("DATE OFFSET: " + new Date(Date.now()).getTimezoneOffset());
    console.log("DATE OFFSET +: " + new Date(Date.now() + new Date(Date.now()).getTimezoneOffset()));
    console.log("DATE OFFSET -: " + new Date(Date.now() - new Date(Date.now()).getTimezoneOffset()));

    var now = moment();
    this.dateWanted = new Date(moment(now.format(), moment.ISO_8601).toDate().getTime()+ new Date().getTimezoneOffset());
    this._dateWanted = new Date();
    console.log("DATE OFFSET moment: "+ moment(now.format(), moment.ISO_8601).format());

    this.description = '';
    this.price = '0';
    this.orderItems = [];
  }

  get dateWanted(): Date {
    return this._dateWanted;
  }

  set dateWanted(value: Date) {
    if (moment(value, OrderModel.DATE_TIME_FORMAT, true).isValid()) {
      console.debug("Date wanted setted on: ", moment(value, OrderModel.DATE_TIME_FORMAT).toDate());
      this._dateWanted = moment(value, OrderModel.DATE_TIME_FORMAT, true).toDate();
    } else if (moment(value, OrderModel.TIME_FORMAT, true).isValid()) {
      console.debug("Date wanted setted on: ", moment(value, OrderModel.TIME_FORMAT).toDate());
      this._dateWanted = moment(value, OrderModel.TIME_FORMAT).toDate();
    } else {
      console.error("Wrong dateformat given to dateWanted in OrderModel: ", value);
    }
  }

  getTotalPrice(): number {
    let price = 0;
    for (let i = 0; i < this.orderItems.length; i++) {
      price = price + this.orderItems[i].getTotalPrice();
    }
    return price;
  }

  getParams(): any[] {
    return [
      this.id,
      this.description,
      this.getTotalPrice(),
      moment(this.datePlaced).format(OrderModel.DATE_TIME_FORMAT),
      moment(this._dateWanted).format(OrderModel.DATE_TIME_FORMAT),
      this.status.status,
      this.firstName,
      this.lastName
    ];
  }
}
