export class OrderStatus {
  static status_in_progress: string = "IN_PROGRESS";
  static status_approved: string = "APPROVED";
  static status_rejected: string = "REJECTED";

  private _status: string;

  get status(): string {
    return this._status;
  }

  constructor(status: string) {
    if (status !== "") {
      this._status = status;
    } else {
      this._status = OrderStatus.status_in_progress;
    }
  }

  approve() {
    console.log("Change orderstatus from " + this._status + " to " + OrderStatus.status_approved);
    this._status = OrderStatus.status_approved;
  }

  reject() {
    console.log("Change orderstatus from " + this._status + " to " + OrderStatus.status_rejected);
    this._status = OrderStatus.status_rejected;
  }

  isInProgress(): boolean {
    return OrderStatus.status_in_progress === this._status;
  }

  isApproved(): boolean {
    return OrderStatus.status_approved === this._status;
  }

  isRejected(): boolean {
    return OrderStatus.status_rejected === this._status;
  }

  toString() {
    switch (this._status) {
      case OrderStatus.status_approved:
        return "Goedgekeurd, tot straks";
      case OrderStatus.status_in_progress:
        return "Wachten op goedkeuring";
      case OrderStatus.status_rejected:
        return "Uw bestelling kan niet op tijd voorzien worden";
    }
  }
}
