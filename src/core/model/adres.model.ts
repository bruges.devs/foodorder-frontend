export class AdresModel {
  street: string;
  city: string;
  postalCode: string;
  houseNumber: string;
  country: string;


  constructor(jsonObject: any) {
    if (jsonObject) {
      this.street = jsonObject.street;
      this.city = jsonObject.city;
      this.postalCode = jsonObject.postalCode;
      this.houseNumber = jsonObject.houseNumber;
      this.country = jsonObject.country;
    }
  }

  getAdresFormatted() {
    return this.postalCode + " " + this.city + ", " + this.street + " " + this.houseNumber;
  }
}
