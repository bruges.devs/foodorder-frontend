export class CategoryModel {

  id: string;
  name: string;
  private _isSelectedAsFilter: boolean = false;

  get isSelectedAsFilter(): boolean {
    return this._isSelectedAsFilter;
  }

  set isSelectedAsFilter(value: boolean) {
    this._isSelectedAsFilter = value;
  }
}
