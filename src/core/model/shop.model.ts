import {AdresModel} from "./adres.model";

export class ShopModel {

  id: string;
  name: string;
  isFavorite: boolean;
  adres: AdresModel;

  constructor(jsonObject?: any) {
    if (jsonObject){
      this.id = jsonObject.id;
      this.name = jsonObject.name;
      this.isFavorite = jsonObject.isFavorite;
      if (this.adres) {
        this.adres = new AdresModel(jsonObject.adres);
      }
    }
  }

  getAdresFormatted(): string{
    if (this.adres) {
      return this.adres.getAdresFormatted();
    }
    return "";
  }

}


