import {ProductModel} from "./product.model";

export class BasketItem {

  product: ProductModel;
  amount: number = 0;
  orderId: number;
  private _ordered: number = 0;

  getTotalPrice(): number {
    return this.product.price * this.amount;
  }

  isOrdered(): boolean {
    return this._ordered == 1;
  }

  set ordered(value: number) {
    this._ordered = value;
  }

  get ordered(): number {
    return this._ordered;
  }
}
