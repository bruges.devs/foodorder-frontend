import {SettingModel} from "./setting.model";
import {SettingTypeEnum} from "../../enum/setting-type.enum";

export class SettingBoolean extends SettingModel {

  type: SettingTypeEnum = SettingTypeEnum.BOOLEAN;
  private _value: boolean;

  get value(): boolean {
    return this._value;
  }

  set value(value: boolean) {
    this._value = value;
  }

  constructor(value: any) {
    super();
    this._value = value === "1";
  }

  getValueForApi(): string {
    return this._value ? "1" : "0";
  }

}
