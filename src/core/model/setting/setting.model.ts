import {SettingTypeEnum} from "../../enum/setting-type.enum";

export abstract class SettingModel {
  id: string;
  key: string;
  abstract type: SettingTypeEnum;
  abstract value: any;
  abstract getValueForApi(): string;
}
