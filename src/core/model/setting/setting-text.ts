import {SettingTypeEnum} from "../../enum/setting-type.enum";
import {SettingModel} from "./setting.model";

export class SettingText  extends SettingModel{

  type: SettingTypeEnum = SettingTypeEnum.TEXT;
  value: string;

  constructor(value: string) {
    super();
    this.value = value;
  }

  getValueForApi(): string {
    return this.value;
  }
}
