import {SettingModel} from "./setting.model";
import {SettingTypeEnum} from "../../enum/setting-type.enum";

export class SettingList extends SettingModel {

  type: SettingTypeEnum = SettingTypeEnum.LIST;
  private _value: string[];

  get value(): string[] {
    return this._value;
  }

  set value(value: string[]) {
    this._value = value;
  }

  constructor(value: string[]) {
    super();
    this._value = value;
  }

  getValueForApi(): string {
    return "";//TODO
  }


}
