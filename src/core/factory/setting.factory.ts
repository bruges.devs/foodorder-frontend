import {SettingModel} from "../model/setting/setting.model";
import {SettingText} from "../model/setting/setting-text";
import {SettingBoolean} from "../model/setting/setting-boolean";
import {SettingNumber} from "../model/setting/setting-number";
import {SettingList} from "../model/setting/setting-list";

export class SettingFactory {
  static createSettingBasedOnType(type: string, value: any): SettingModel {
    switch (type) {
      case "TEXT":
        return new SettingText(value);
      case "BOOLEAN":
        return new SettingBoolean(value);
      case "NUMBER":
        return new SettingNumber(value);
      case "LIST":
        return new SettingList(value);
      default:
        return new SettingText(value);

    }
  }
}
