import {Injectable} from "@angular/core";
import {Category, CategoryControllerService} from "../../providers";
import {LoggingService} from "../shared/service/logging.service";
import {LogLevel} from "../shared/constants/loglevel";
import {Events} from "ionic-angular";
import {EventChannels} from "../constants/event-channels";
import {CategoryModel} from "../model/category.model";
import {MapperCategory} from "../mapper/api/mapper.category";

@Injectable()
export class CategoryService {

  constructor(private categoryControllerService: CategoryControllerService,
              private logger: LoggingService,
              private events: Events) {}

  loadCategoriesFromApi() {
    this.logger.log(LogLevel.FINE, "Load categories from API", null);
    this.categoryControllerService.getCategoriesUsingGET().toPromise()
      .then((categories: Category[]) => new MapperCategory().mapList(categories))
      .then((categories: CategoryModel[]) => this.events.publish(EventChannels.CATEGORIES_LOADED, categories));
  }

  loadCategoriesFromDB() {
    this.logger.log(LogLevel.FINE, "Load categories from DB", null);
    //TODO LOAD CATEOGRIES FROM DB
  }
}
