import {Injectable} from "@angular/core";
import {Product, ProductControllerService} from "../../providers";
import "rxjs/add/operator/map";
import {Observable} from "rxjs/Observable";
import {ProductDao} from "../dao/product.dao";
import {LoggingService} from "../shared/service/logging.service";
import {LogLevel} from "../shared/constants/loglevel";
import {ProductModel} from "../model/product.model";
import {RowMapperProduct} from "../mapper/db/row-mapper.product";
import {MapperProduct} from "../mapper/api/mapper.product";

@Injectable()
export class ProductService {

  constructor(private productController: ProductControllerService,
              private productDao: ProductDao,
              private logger: LoggingService) {
    this.productController = productController;
  }

  loadProducts(): Promise<ProductModel[]> {
    return this.productController.getProductsUsingGET()
      .map(items => {
        this.logger.log(LogLevel.DEBUG,"Retrieved products from service: ", items);
        return new MapperProduct().mapList(items);
      }).toPromise();
  }

  loadProduct(id: number): Observable<ProductModel> {
    return this.productController.getProductUsingGET(id)
      .map(item => {
        this.logger.log(LogLevel.DEBUG, "Retrieved product from service: ", item);
        return new MapperProduct().mapTo(item);
      });
  }

  saveProducts(products: ProductModel[]): Promise<boolean> {
    return this.productDao.saveOrReplaceProducts(products)
      .then((isSaved: boolean) => {
        this.logger.log(LogLevel.DEBUG, "Producten zijn opgeslagen: ", isSaved);
        return isSaved;
      });
  }
}
