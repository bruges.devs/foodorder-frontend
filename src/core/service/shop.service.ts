import {Injectable} from "@angular/core";
import {LoggingService} from "../../core/shared/service/logging.service";
import {Events} from "ionic-angular";
import {ShopModel} from "../model/shop.model";
import {AdresModel} from "../model/adres.model";

@Injectable()
export class ShopService {

  constructor(private logger: LoggingService,
              private events: Events) {
  }

  public loadShops(): Promise<ShopModel[]> {
    return new Promise<ShopModel[]>((resolve, reject) => {
      //TODO HACK
      let names = ["Faluche", "Luc", "Snackstje", "Broodjes om de hoek", "Johan's lunch", "De Gentenoar"];
      let cities = ["Brugge", "Brugge", "Brugge", "Sint-Michiels", "Sint-Andries", "Gent"];
      let postalCodes = ["8000", "8000", "8000", "8200", "8200", "9000"];
      let adres;

      let shop;
      let shops = [];
      for (let i = 0; i < names.length; i++) {
        adres = new AdresModel(null);
        adres.city = cities[i];
        adres.postalCode = postalCodes[i];
        adres.street = "Street" + i;
        adres.country = "België";
        adres.houseNumber = i;

        shop = new ShopModel();
        shop.name = names[i];
        shop.isFavorite = i % 2 === 0;
        shop.adres = adres;
        shops.push(shop);
      }
      resolve(shops);
      //TODO END HACK
    });
  }
}
