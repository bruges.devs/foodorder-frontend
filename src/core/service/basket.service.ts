import {Injectable} from "@angular/core";
import {LoggingService} from "../shared/service/logging.service";
import {BasketDao} from "../dao/basket.dao";
import {LogLevel} from "../shared/constants/loglevel";
import {EventChannels} from "../constants/event-channels";
import {Events} from "ionic-angular";
import {MyToastService} from "../shared/service/my-toast.service";
import {BasketItem} from "../model/basket-item";
import {ProductModel} from "../model/product.model";

@Injectable()
export class BasketService {

  constructor(private logger: LoggingService,
              private basketDao: BasketDao,
              private events: Events,
              private toastCtrl: MyToastService) {
  }

  loadBasket(): void {
    this.basketDao.loadBasket()
      .then((basketItems: BasketItem[]) => this.events.publish(EventChannels.BASKET_LOADED, basketItems));
  }

  removeProductFromBasket(product: ProductModel): void {
    this.basketDao.emptyBasket(product)
      .then((isRemoved: boolean) => {
        if (isRemoved) {
          this.events.publish(EventChannels.BASKET_ITEM_REMOVED, product);
          this.events.publish(EventChannels.BASKET_UPDATED, null);
          this.toastCtrl.showToast(product.name + " verwijderd uit winkelmandje", 2000);
          this.logger.log(LogLevel.DEBUG, "Product verwijderd uit winkelmandje: ", product);
        } else {
          this.toastCtrl.showErrorToast(product.name + " kon niet verwijderd worden uit winkelmandje", 2000);
        }
      });
  }

  addToBasket(product: ProductModel): void {
    this.basketDao.getBasketItemByProductId(product)
      .then((basketItem: BasketItem) => {
        if (basketItem === null) {
          this.insertIntoBasket(product);
        } else {
          basketItem.product = product;
          basketItem.amount = basketItem.amount + 1;
          this.updateIntoBasket(basketItem);
        }
      });
  }

  removeFromBasket(basketItem: BasketItem) {
    if (basketItem.amount === 1) {
      this.removeProductFromBasket(basketItem.product);
    } else {
      basketItem.amount = basketItem.amount - 1;
      this.updateIntoBasket(basketItem);
    }
  }

  updateBasketCount(): void {
    this.basketDao.getBasketItems()
      .then((basketItems: BasketItem[]) => {
        let count = 0;
        for (let i = 0; i < basketItems.length; i++) {
          count = count + basketItems[i].amount;
        }
        this.events.publish(EventChannels.BASKET_COUNT_UPDATED, count);
      });
  }

  insertIntoBasket(product: ProductModel) {
    this.basketDao.addInBasket(product)
      .then((isSaved: boolean) => {
        if (isSaved) {
          let basketItem = new BasketItem();
          basketItem.amount = 1;
          basketItem.product = product;

          this.events.publish(EventChannels.BASKET_ITEM_ADDED, basketItem);
          this.events.publish(EventChannels.BASKET_UPDATED, null);
          this.logger.log(LogLevel.DEBUG, "Product added to basket: ", basketItem);
        } else {
          this.toastCtrl.showErrorToast(product.name + " kon niet toegevoegd worden aan winkelmandje", 2000);
        }
      });
  }

  private updateIntoBasket(basketItem: BasketItem) {
    this.basketDao.updateBasket(basketItem)
      .then((isUpdated: boolean) => {
        if (isUpdated) {
          this.events.publish(EventChannels.BASKET_ITEM_UPDATED, basketItem);
          this.events.publish(EventChannels.BASKET_UPDATED, null);
          this.logger.log(LogLevel.DEBUG, "Product updated into basket: ", basketItem.product);
          //TODO TOAST OR VISUAL MESSAGE
        } else {
          this.toastCtrl.showErrorToast(basketItem.product.name + " kon niet aangepast worden aan winkelmandje", 2000);
        }
      });
  }

}
