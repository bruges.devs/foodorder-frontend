import {Injectable} from "@angular/core";
import {OrderControllerService} from "../../providers/api/orderController.service";
import {OrderModel} from "../model/order.model";
import {MapperBasketItemToOrderItem} from "../mapper/api/mapper.basket.-item.to.order-item";
import {LoggingService} from "../shared/service/logging.service";
import {LogLevel} from "../shared/constants/loglevel";
import {OrderDao} from "../dao/order.dao";
import {Events} from "ionic-angular";
import {EventChannels} from "../constants/event-channels";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {BasketItem} from "../model/basket-item";
import {BasketDao} from "../dao/basket.dao";
import {OrderStatus} from "../model/order-status";

@Injectable()
export class OrderService {

  constructor(private orderControllerService: OrderControllerService,
              private loggingService: LoggingService,
              private orderDao: OrderDao,
              private basketDao: BasketDao,
              private httpClient: HttpClient,
              private events: Events) {
  }

  loadOrders(): void {
    this.orderDao.loadOrders()
      .then((orders: OrderModel[]) => this.loadFullOrders(orders))
      .then((orders: OrderModel[]) => orders.sort((a, b) => this.sortOrderOnDate(a, b)))
      .then((orders: OrderModel[]) => this.events.publish(EventChannels.ORDERS_LOADED, orders));
  }

  placeOrder(order: OrderModel): Promise<boolean> {
    let oneSignalUserId = window.localStorage.getItem('ONE_SIGNAL_USER_ID');//TODO PASS IN PLCAE ORDER

    let createOrderRequest: any = {
      orderItems: new MapperBasketItemToOrderItem().mapList(order.orderItems),
      pushNotificationId: oneSignalUserId,
      firstName: order.firstName,
      lastName: order.lastName,
      dateWanted: order.dateWanted
    };

    this.loggingService.log(LogLevel.DEBUG, "Order will be sent to backend: ", createOrderRequest);

    return this.orderControllerService.createUsingPOST(createOrderRequest).toPromise()
      .then((response: { id, status }) => {
        this.loggingService.log(LogLevel.FINE, "Response from createUsingPOST: ", response);
        this.loggingService.log(LogLevel.DEBUG, "CreateOrderRequest placed and saved server side: ", createOrderRequest);
        return response;
      }).then((response: { id, status }) => {
        let orderToSave = new OrderModel();
        orderToSave.id = response.id;
        orderToSave.status = new OrderStatus(response.status);
        orderToSave.orderItems = order.orderItems;
        orderToSave.firstName = order.firstName;
        orderToSave.lastName = order.lastName;
        orderToSave.dateWanted = order.dateWanted;
        orderToSave.price = order.price;
        orderToSave.datePlaced = order.datePlaced;

        return this.saveOrder(orderToSave)
          .then((savedOrderId: number) => {
            if (savedOrderId != -1) {
              return this.orderDao.updateOrderItems(order.orderItems, savedOrderId);
            }
            return false;
          });
      }).catch(reason => {
        this.loggingService.log(LogLevel.ERROR, "Dit order kon niet geplaatst worden: ", createOrderRequest);
        return false;
      });
  }

  saveOrder(order: OrderModel): Promise<number> {
    return this.insertOrder(order);
  }

  checkIfOrderStatusChanged(orders: OrderModel[]) {
    let filteredOrders = orders.filter((order: OrderModel) => order.status.isInProgress());
    let orderByIdObservables = [];

    filteredOrders.forEach((orderInProgress: OrderModel) => {
      orderByIdObservables.push(this.orderControllerService.getOrderUsingGET(orderInProgress.id));
    });

    Observable.forkJoin(orderByIdObservables).toPromise().then((loadedOrders: { id, status }[]) => {
      this.loggingService.log(LogLevel.FINE, "Loaded orders from backend:", loadedOrders);

      loadedOrders.forEach((loadedOrder: { id, status }) => {
        let orderToSetStatus = orders.filter(value => value.id === loadedOrder.id);
        if (orderToSetStatus.length === 1) {
          if (loadedOrder.status === OrderStatus.status_approved) {
            orderToSetStatus[0].status.approve();
          } else if (loadedOrder.status === OrderStatus.status_rejected) {
            orderToSetStatus[0].status.reject();
          }
        } else {
          this.loggingService.log(LogLevel.ERROR, "Order with id not found: ", loadedOrder);
        }
      });
    }).catch(reason => this.loggingService.log(LogLevel.ERROR, "De status van de orders kon niet opgehaald worden:", reason));
  }

  private loadFullOrders(orders: OrderModel[]): Promise<OrderModel[]> {
    if (orders.length === 0) {
      return new Promise<OrderModel[]>((resolve, reject) => {
        resolve([]);
      });
    }

    let observableBatch: Observable<BasketItem[]>[] = [];

    orders.forEach((order: OrderModel) => {
      observableBatch.push(Observable.fromPromise(this.basketDao.getBasketItemsByOrderId(order.id)));
    });

    return Observable.forkJoin(observableBatch).map((basketItems: BasketItem[][]) => {
      let loadedBasketItems = [].concat.apply([], basketItems);
      this.loggingService.log(LogLevel.FINE, "All basketItems for orders found: ", [orders, loadedBasketItems]);

      orders.forEach((order: OrderModel) => {
        order.orderItems = loadedBasketItems.filter((_: BasketItem) => _.orderId === order.id);
        this.loggingService.log(LogLevel.FINE, "Added orderItems to order as orderItems: ", order);
      });

      return orders;
    }).toPromise();
  }

  private insertOrder(order: OrderModel): Promise<number> {
    return this.orderDao.insertOrder(order).then((insertedOrder: OrderModel) => {
      if (insertedOrder.id) {
        this.loggingService.log(LogLevel.DEBUG, "Order inserted in DB: ", insertedOrder);
        return insertedOrder.id;
      } else {
        this.loggingService.log(LogLevel.ERROR, "Failed to insert order: ", order);
        return -1;
      }
    });
  }

  private updateOrder(order: OrderModel): Promise<boolean> {
    return this.orderDao.updateOrder(order)
      .then((isUpdated: boolean) => {
        if (isUpdated) {
          this.loggingService.log(LogLevel.DEBUG, "Order updated in DB: ", order);
        } else {
          this.loggingService.log(LogLevel.ERROR, "Failed to update order: ", order);
        }
        return isUpdated;
      });
  }

  private sortOrderOnDate(a: OrderModel, b: OrderModel): number {
    return a.datePlaced.getTime() < b.datePlaced.getTime() ? 1 : -1;
  }

}
