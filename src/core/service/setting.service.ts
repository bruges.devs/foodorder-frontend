import {Injectable} from "@angular/core";
import {SettingControllerService} from "../../providers/api/settingController.service";
import {Setting} from "../../providers/model/setting";
import {LoggingService} from "../shared/service/logging.service";

@Injectable()
export class SettingService {

  constructor(private settingsController: SettingControllerService,
              private logger: LoggingService) {
  }

  loadSettingByKey(key: string): Promise<Setting> {
    return this.settingsController.getSettingByKeyUsingGET(key).toPromise();
  }
}
