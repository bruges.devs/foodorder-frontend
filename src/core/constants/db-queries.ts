export class DbQueries {

  /**
   * Database query for inserting a db_version record.
   */
  static INSERT_INITIAL_DB_VERSION: string = 'INSERT INTO db_version(version, type, modifyDate) values(?, ?, ?);';

  /**
   * Database query for getting the Database version
   */
  static GET_DB_VERSION: string = 'SELECT * FROM db_version';

  /**
   * Database query for updating the Database version. This is being used during app startup, when database modifications have been defined.
   */
  static UPDATE_DB_VERSION: string = 'UPDATE DB_VERSION set version=?, type=?, modifyDate=?';

}
