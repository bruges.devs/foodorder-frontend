export class EventChannels {

  //GENERAL
  static NETWORK_CHANNEL = "network:channel";
  static CHANNEL_BACKEND_NOT_AVAILABLE = 'backend:not-available';

  //PRODUCTS
  static PRODUCTS_LOADED = 'PRODUCTS_LOADED';
  static PRODUCTS_LOADED_FAILED = 'PRODUCTS_LOADED_FAILED';

  //CATEGORY
  static CATEGORIES_LOADED = 'CATEGORIES_LOADED';
  static CATEGORIES_FILTER = 'CATEGORIES_FILTER';

  //BASKET
  static BASKET_UPDATED = 'basket:updated';
  static BASKET_ITEM_ADDED = "basket:item:added";
  static BASKET_ITEM_REMOVED = "basket:item:removed";
  static BASKET_COUNT_UPDATED = "basket:count:updated";
  static BASKET_PRODUCT_COUNT_UPDATED = "basket:product:count:updated";
  static BASKET_ITEM_UPDATED: "basket:item:updated";
  static BASKET_LOADED = "basket:loaded";

  static BASKET_VISUAL_COUNT = "basket:visual:count";

  //ORDERS
  static ORDERS_LOADED = "orders:loaded";
  static ORDERS_STATUS_CHANGED = "orders:status:changed";

  //SHOPS
  static SHOP_LOADED = "shop:loaded";

  //SETTINGS
  static SETTINGS_SHOW = 'settings:show';

}
