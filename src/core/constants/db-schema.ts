export class DbSchema {

  public static CREATE_TABLE_DB_VERSION: string = `CREATE TABLE \`db_version\` (
        \`dbVersionId\`	INTEGER NOT NULL,
        \`version\`	TEXT NOT NULL,
        \`type\`	TEXT NOT NULL,
        \`modifyDate\`	INTEGER NOT NULL,
        PRIMARY KEY(dbVersionId)
    );`;

  public static CREATE_TABLE_BASKET: string = `CREATE TABLE \`tbl_basket\` (
        \`id\`	INTEGER NOT NULL,
        \`product_id\`	INTEGER NOT NULL,
        \`amount\` INTEGER NOT NULL,
        \`is_ordered\` INTEGER NOT NULL,
        \`order_id\` INTEGER,
        PRIMARY KEY(id)
    );`;

  public static CREATE_TABLE_PRODUCT: string = `CREATE TABLE \`tbl_product\` (
        \`id\`	INTEGER NOT NULL,
        \`category_id\` TEXT NOT NULL,
        \`category_name\` TEXT NOT NULL,
        \`description\` TEXT NOT NULL,
        \`name\` TEXT NOT NULL,
        \`price\` TEXT NOT NULL,
        PRIMARY KEY(id)
    );`;

  public static CREATE_TABLE_ORDER: string = `CREATE TABLE \`tbl_order\` (
        \`id\`	INTEGER NOT NULL,     
        \`description\` TEXT NOT NULL,
        \`price\` TEXT NOT NULL,
        \`date_placed\` TEXT NOT NULL,
        \`date_wanted\` TEXT NOT NULL,
        \`status\` TEXT NOT NULL,
        \`first_name\` TEXT NOT NULL,
        \`last_name\` TEXT NOT NULL,
        PRIMARY KEY(id)
    );`;
}
