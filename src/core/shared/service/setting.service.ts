import {Injectable} from "@angular/core";
import {LogLevel} from "../constants/loglevel";
import {LoggingService} from "./logging.service";
import {SettingModel} from "../../model/setting/setting.model";
import {Setting} from "../../../providers/model/setting";
import {SettingControllerService} from "../../../providers/api/settingController.service";
import {MapperSetting} from "../../mapper/mapper.setting";

@Injectable()
export class SettingService {

  constructor(private settingsController: SettingControllerService,
              private logger: LoggingService) {
  }

  loadSettings(): Promise<SettingModel[]> {
    return this.settingsController.getSettingsUsingGET().toPromise()
      .then((settings: Setting[]) => {
        let mappedSettings: SettingModel[] = new MapperSetting().mapList(settings);
        this.logger.log(LogLevel.DEBUG, "Settings Loaded from service: ", mappedSettings);
        return mappedSettings;
      }).catch(reason => {
        this.logger.log(LogLevel.ERROR, "Failed to load settings from API: ", reason);
        return [];
      });
  }
}
