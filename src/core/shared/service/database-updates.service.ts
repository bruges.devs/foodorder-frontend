import {Injectable} from "@angular/core";
import "rxjs/add/observable/forkJoin";
import "rxjs/add/observable/from";
import "rxjs/add/observable/fromPromise";
import "rxjs/add/operator/mergeMap";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/map";
import "rxjs/add/observable/empty";
import _ from "lodash";
import {isNullOrUndefined} from "util";
import {HttpClient} from "@angular/common/http";
import {LoggingService} from "./logging.service";
import {DatabaseService, DatabaseStatement} from "./database.service";
import {Observable} from "rxjs/Observable";
import {LogLevel} from "../constants/loglevel";
import {DbQueries} from "../../constants/db-queries";
import {DbVersion} from "../mapper/mapper.dbversion.db";


@Injectable()
export class DatabaseUpdatesService {

  private dbUpdates: Array<Observable<any>>;

  constructor(private http: HttpClient,
              private logger: LoggingService,
              private databaseService: DatabaseService<any>) {

    this.dbUpdates = [];

    // this.dbUpdates.push(this.http.get('assets/db-updates/v1.1_question_with_answers_for_ipa_and_contact.json'));
  }


  //TODO getUpdateScripts() handeld zichzelf niet af als er Observable.empty() teruggegeven wordt
  updateDatabaseWithScripts(): Promise<boolean> {
    this.logger.log(LogLevel.DEBUG, "updateDatabaseWithScripts - running update scripts", undefined);
    let that = this;

    return new Promise((resolve, reject) => {
      this.databaseService.getDatabaseVersion().then(function (dbVersion) {
        that.getUpdateScripts(dbVersion).subscribe((scripts) => {
          that.logger.log(LogLevel.DEBUG, "updateDatabaseWithScripts - scripts", scripts);
          if (isNullOrUndefined(scripts) || scripts.length === 0) {
            resolve(true);
          } else {
            that.updateDatabase(scripts).then((result) => {
              that.logger.log(LogLevel.DEBUG, "Updating db version to new version", undefined);
              let newDbVersion = that.getNewDbVersion(dbVersion.version, scripts);
              let updateStatement = new DatabaseStatement(DbQueries.UPDATE_DB_VERSION, [newDbVersion, "DB_UPDATE", new Date().getTime()]);
              that.databaseService.executeSql(updateStatement);
              resolve(true);
            }).catch((err) => {
              console.log(err);
              resolve(false);
            });
          }
        });
      });
    });
  }

  private updateDatabase(scripts: Array<DbUpdate>): Promise<boolean> {
    this.logger.log(LogLevel.FINE, "executing scripts: ", scripts);

    let queries: Array<DatabaseStatement> = [];
    for (let i = 0; i < scripts.length; i++) {
      let script: DbUpdate = scripts[i];
      this.logger.log(LogLevel.FINE, "Looping db update", script);
      for (let j = 0; j < script.schemaChanges.length; j++) {
        let schemaChange = script.schemaChanges[j];
        queries.push(new DatabaseStatement(schemaChange.query, schemaChange.parameters));
      }
    }

    this.logger.log(LogLevel.FINE, "Executing queries", queries);
    return this.databaseService.executeBatch(queries);
  }

  private getUpdateScripts(currentDbVersion: DbVersion): Observable<any> {
    this.logger.log(LogLevel.DEBUG, "Getting update scripts", this.dbUpdates);
    let dbVersionNumber: number = parseFloat(currentDbVersion.version);
    this.logger.log(LogLevel.INFO, "Getting update scripts above version", dbVersionNumber);

    if (this.dbUpdates.length === 0) {
      this.logger.log(LogLevel.INFO, "Db updates length ", this.dbUpdates.length);
      return Observable.empty();//TODO ZORGT DAT DE OBSERVABLE NIET VERDER LOOPT
    }

    return Observable.forkJoin(this.dbUpdates)
      .flatMap(results => {
        this.logger.log(LogLevel.DEBUG, "Scripts to filter and sort", results);
        results = _.filter(results, (dbUpdate: DbUpdate) => dbUpdate.schemaVersion > dbVersionNumber);
        this.logger.log(LogLevel.INFO, "Scripts after filtering", results.length);
        return Observable.from(results.sort(this.sortUpdateScripts));
      }).toArray();
  }

  private getNewDbVersion(currentDbVersion: string, result: Array<DbUpdate>): number {
    let version: number = parseFloat(currentDbVersion);

    for (let i = 0; i < result.length; i++) {
      if (result[i].schemaVersion > version) {
        version = result[i].schemaVersion;
      }
    }

    return version;
  }

  private sortUpdateScripts = (script1, script2) => {
    this.logger.log(LogLevel.DEBUG, "Sorting update scripts", [script1, script2]);
    if (script1.schemaVersion > script2.schemaVersion) {
      return 1;
    }

    if (script1.schemaVersion < script2.schemaVersion) {
      return -1;
    }

    return 0;
  };

}

export class DbUpdateStatement {

  public query: string;
  public parameters: Array<any>

}

export class DbUpdate {
  public schemaVersion: number;
  public schemaChanges: Array<DbUpdateStatement>;
}

