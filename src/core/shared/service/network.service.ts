import {Injectable, OnDestroy} from "@angular/core";
import {Subscription} from "rxjs/Subscription";
import {Events, Platform} from "ionic-angular";
import {LoggingService} from "./logging.service";
import {Network} from "@ionic-native/network";
import {HealthService} from "./health.service";
import {EventChannels} from "../../constants/event-channels";
import {LogLevel} from "../constants/loglevel";
import {MyToastService} from "./my-toast.service";

declare var navigator: any;

@Injectable()
export class NetworkService implements OnDestroy {

  private _hasNetworkConnection: boolean = true;
  private _hasBackEndConnection: boolean = true;

  private subscriptions: Array<Subscription> = [];

  constructor(private events: Events,
              private network: Network,
              private platform: Platform,
              private healthService: HealthService,
              private toastCtrl: MyToastService,
              private logger: LoggingService) {

    this._hasNetworkConnection = this.determineConnection(navigator.connection.type);

    this.subscriptions.push(
      this.network.onDisconnect().subscribe(() => {
        this.logger.log(LogLevel.WARN, 'Disconnected: ', true);
        this._hasNetworkConnection = false;
        this.events.publish(EventChannels.NETWORK_CHANNEL, this._hasNetworkConnection);
      })
    );

    this.subscriptions.push(
      this.network.onConnect().subscribe(() => {
        this.logger.log(LogLevel.INFO, 'Connected: ', true);

        //CHECK BACKEND EVERY 5 MINUTES
        setTimeout(() => {
          if (this.determineConnection(navigator.connection.type)) {
            this._hasNetworkConnection = true;
            this.events.publish(EventChannels.NETWORK_CHANNEL, this.hasConnection());
          }
        }, 1000* 6000);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  initializeNetworkStatus(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.logger.log(LogLevel.INFO, "Network initialized", undefined);
      this._hasNetworkConnection = this.determineConnection(navigator.connection.type);
      this.logger.log(LogLevel.DEBUG, "Initialize network status returns: ", this._hasNetworkConnection);
      resolve(this._hasNetworkConnection);
      // setInterval(() => {
      //     this.healthService.isBackendAvailable().then((result) => {
      //         this._hasBackEndConnection = result;
      //         this.events.publish(EventChannels.NETWORK_CHANNEL, this.hasConnection());
      //         resolve(this.hasConnection());
      //     });
      // }, 60 * 1000);
      //
      // this.healthService.isBackendAvailable().then((result) => {
      //     this._hasBackEndConnection = result;
      //     resolve(this.hasConnection());
      // });
    });
  }

  public hasConnection(): boolean {
    if (!this._hasNetworkConnection && !this._hasBackEndConnection) {
      this.toastCtrl.showToast("Probeer te verbinden met het internet", 2000);
    }
    return this._hasNetworkConnection && this._hasBackEndConnection;
  }

  private determineConnection(connection: any): boolean {
    this.logger.log(LogLevel.DEBUG, "determineConnection", connection);

    if (this.isBrowser()) {
      return true;
    } else {
      return connection === '4g' ||
        connection === '3g' ||
        connection === 'wifi';
    }
  }

  private isBrowser(): boolean {
    return this.platform.is('core') || this.platform.is('mobileweb');
  }

}
