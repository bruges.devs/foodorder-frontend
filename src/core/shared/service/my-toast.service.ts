import {Injectable} from "@angular/core";
import {ToastController} from "ionic-angular";

@Injectable()
export class MyToastService {


  constructor(private toastCtrl: ToastController) {
  }

  showToast(message: string, duration: number){
    const toast = this.toastCtrl.create({
      message: message,
      duration: duration
    });
    toast.present();
  }

  showErrorToast(message: string, duration: number) {
    //TODO RED COLOR
    const toast = this.toastCtrl.create({
      message: message,
      duration: duration
    });
    toast.present();
  }
}
