import {Injectable} from "@angular/core";
import {AlertController} from "ionic-angular";
import {TranslateService} from "@ngx-translate/core";

@Injectable()
export class DialogService {

  constructor(private translateService: TranslateService, private alertController: AlertController) {}

  public showDialogWithOK(titleKey: string, messageKey: string) {
    let confirm = this.alertController.create({
      title: this.translateService.instant(titleKey),
      message: this.translateService.instant(messageKey),
      buttons: [{
        text: this.translateService.instant("GENERAL.OK"),
        handler: () => {
          // user has clicked the alert button
          // begin the alert's dismiss transition
          confirm.dismiss();
          return false;
        }
      }]
    });
    confirm.present();
  }

}
