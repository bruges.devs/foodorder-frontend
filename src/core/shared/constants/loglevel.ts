export enum LogLevel {
  FINE,
  DEBUG,
  INFO,
  WARN,
  ERROR
}
