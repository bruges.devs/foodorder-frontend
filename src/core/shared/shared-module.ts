import {ErrorHandler, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {IonicErrorHandler} from 'ionic-angular';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Network} from "@ionic-native/network";
import {HttpClientModule} from "@angular/common/http";
import {NetworkService} from "./service/network.service";
import {DatabaseUpdatesService} from "./service/database-updates.service";
import {LoadingDialogService} from "./service/loading-dialog.service";
import {HealthService} from "./service/health.service";
import {FirstStartupService} from "./service/first-startup.service";
import {MyToastService} from "./service/my-toast.service";
import {DatabaseService} from "./service/database.service";
import {DialogService} from "./service/dialog.service";
import {LoggingService} from "./service/logging.service";
import {AppCreator} from "./app/app-creator";

@NgModule({
  declarations: [],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  bootstrap: [],
  entryComponents: [],
  providers: [
    StatusBar,
    SplashScreen,
    LoggingService,
    DialogService,
    DatabaseService,
    NetworkService,
    LoadingDialogService,
    DatabaseUpdatesService,
    FirstStartupService,
    HealthService,
    MyToastService,
    Network,
    AppCreator,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class SharedModule {
}
