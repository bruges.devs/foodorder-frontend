export interface RowsMapper<T> {

  mapList(from: any, index: number): T[];

}
