export interface RowMapper<T> {

  mapRow(from: any, index: number): T;

}
