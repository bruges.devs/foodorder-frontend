export interface AppCreatorListener {
  initializeAppAfterErrors();
  appIsPreparedHandler(): Promise<boolean>;
}
