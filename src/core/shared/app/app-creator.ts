import {Injectable} from "@angular/core";
import {LogLevel} from "../constants/loglevel";
import {DatabaseService} from "../service/database.service";
import {NetworkService} from "../service/network.service";
import {DatabaseUpdatesService} from "../service/database-updates.service";
import {FirstStartupService} from "../service/first-startup.service";
import {LoggingService} from "../service/logging.service";
import {AppCreatorListener} from "./app-creator-listener";
import {OrderFacade} from "../../../pages/facade/order.facade";

@Injectable()
export class AppCreator {

  private _appCreatorListener: AppCreatorListener;
  private _orderInterval = 0;

  set appCreatorListener(value: AppCreatorListener) {
    this._appCreatorListener = value;
  }

  constructor(private databaseUpdatesService: DatabaseUpdatesService,
              private firstStartupService: FirstStartupService,
              private databaseService: DatabaseService<any>,
              private orderFacade: OrderFacade,
              private networkService: NetworkService,
              private logger: LoggingService) {
  }

  public prepareApp(): Promise<boolean> {
    return this.firstStartupService.isFirstStartup()
      .then((firstStartup) => {
        this.logger.log(LogLevel.INFO, "First startup", firstStartup);
        if (firstStartup) {
          return this.createDatabase();
        } else {
          return true;
        }
      })
      //TODO getUpdateScripts() handeld zichzelf niet af als er Observable.empty() teruggegeven wordt
      //.then(() => this.databaseUpdatesService.updateDatabaseWithScripts())
      .then(() => this.networkService.initializeNetworkStatus())
      .then(() => this.listenIfOrderChanged())
      .then(() => this._appCreatorListener.appIsPreparedHandler());
  }

  private _handleAppStartupErrors = (error) => {
    this.logger.log(LogLevel.ERROR, "App startup failed", error);

    if (error.code && error.code === 1000) {
      this.logger.log(LogLevel.ERROR, 'handleAppStartupErrors - error', error);
      setTimeout(() => {
        this._appCreatorListener.initializeAppAfterErrors()
      }, 10000);
    }
  };

  private createDatabase(): Promise<boolean> {
    return this.databaseService.createDatabaseDuringFirstStartup()
      .then((isCreated: boolean) => {
        if (isCreated) {
          this.logger.log(LogLevel.INFO, "Created database: ", true);
        } else {
          this.logger.log(LogLevel.ERROR, "Failed to create database: ", "");
        }
        return true;
      });
  }

  /**
   * Elke 3 minuten status update
   * */
  private listenIfOrderChanged() {
    //REFRESH OM DE 3 MINUTEN
    this._orderInterval = setInterval(() => {
      this.orderFacade.loadOrdersFromDatabase();
      this.orderFacade.checkOrderStatusChanged();
    }, 180 * 1000);
  }

  stopOrderChangeListening() {
    clearInterval(this._orderInterval);
  }
}
