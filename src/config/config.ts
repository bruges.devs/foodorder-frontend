import {LogLevel} from "../core/shared/constants/loglevel";

export class Config {

  public static SERVER_APP_URL = "https://localhost:8081/api";

  public static LOG_LEVELS: Array<LogLevel> = [LogLevel.FINE, LogLevel.DEBUG, LogLevel.INFO, LogLevel.WARN, LogLevel.ERROR];

  public static IS_IN_DEBUG = true;

}

export class LocalStorageIds {
  public static ONE_SIGNAL_USER_ID = "ONE_SIGNAL_USER_ID";
}
