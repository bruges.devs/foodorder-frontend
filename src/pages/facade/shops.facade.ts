import {Injectable} from "@angular/core";
import {LoggingService} from "../../core/shared/service/logging.service";
import {Events} from "ionic-angular";
import {ShopModel} from "../../core/model/shop.model";
import {ShopService} from "../../core/service/shop.service";

@Injectable()
export class ShopsFacade {

  shops: ShopModel[] = [];
  filteredShops: ShopModel[] = [];
  isFilterActive: boolean = false;

  constructor(private logger: LoggingService,
              private events: Events,
              private shopService: ShopService) {
  }

  loadShops(): Promise<ShopModel[]> {
    this.isFilterActive = false;
    return this.shopService.loadShops()
      .then((shops: ShopModel[]) => {
        this.shops = shops;
        return shops;
      });
  }

  showFavoriteShops(showFavorite: boolean) {
    this.isFilterActive = showFavorite;
    this.filteredShops = this.shops.filter((shop: ShopModel) => shop.isFavorite === showFavorite);
  }

  hasChosenShop(): boolean {
    return false;//TODO
  }

  filterShops(searchString: string) {
    this.filteredShops = this.shops
      .filter((shop: ShopModel) => shop.isFavorite === this.isFilterActive)
      .filter((shop: ShopModel) => shop.name.toLowerCase().indexOf(searchString.toLowerCase().trim()) >= 0
        || shop.getAdresFormatted().toLowerCase().indexOf(searchString.toLowerCase().trim()) >= 0);
  }

  noFilter() {
    this.filteredShops = this.shops.filter((shop: ShopModel) => shop.isFavorite === this.isFilterActive);
  }
}
