import {Injectable} from "@angular/core";
import {LoggingService} from "../../core/shared/service/logging.service";
import {Events} from "ionic-angular";
import {EventChannels} from "../../core/constants/event-channels";
import {OrderModel} from "../../core/model/order.model";
import {OrderService} from "../../core/service/order.service";
import {LogLevel} from "../../core/shared/constants/loglevel";

@Injectable()
export class OrderFacade {

  orders: OrderModel[] = [];

  constructor(private logger: LoggingService,
              private orderService: OrderService,
              private events: Events) {

    this.events.subscribe(EventChannels.ORDERS_LOADED, (loadedOrders: OrderModel[]) => {
      this.logger.log(LogLevel.DEBUG, "Orders loaded: ", loadedOrders);
      this.orders = loadedOrders;
      this.checkOrderStatusChanged();
    });
  }

  loadOrdersFromDatabase() {
    this.orderService.loadOrders();
  }

  checkOrderStatusChanged(){
    /**Als er geen orders zijn dan moeten we geen API calls doen**/
    if (this.orders.length !== 0){
      this.orderService.checkIfOrderStatusChanged(this.orders);
    }
  }
}
