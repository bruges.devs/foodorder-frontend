import {Injectable} from "@angular/core";
import {SettingService} from "../../core/service/setting.service";
import {Events} from "ionic-angular";
import {Setting} from "../../providers/model/setting";

@Injectable()
export class SettingFacade {

  constructor(private settingService: SettingService,
              private events: Events) {
  }

  loadSettingByKey(key: string): Promise<boolean> {
    return this.settingService.loadSettingByKey(key)
      .then((setting: Setting) => {
        localStorage.setItem('CAN_APPROVE', setting.value);
        return true;
      });
  }
}
