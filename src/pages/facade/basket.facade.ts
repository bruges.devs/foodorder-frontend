import {EventChannels} from "../../core/constants/event-channels";
import {Events} from "ionic-angular";
import {BasketService} from "../../core/service/basket.service";
import {ProductService} from "../../core/service/product.service";
import {LoggingService} from "../../core/shared/service/logging.service";
import {LogLevel} from "../../core/shared/constants/loglevel";
import {Injectable} from "@angular/core";
import {BasketItem} from "../../core/model/basket-item";
import {OrderService} from "../../core/service/order.service";
import {OrderModel} from "../../core/model/order.model";
import {NetworkService} from "../../core/shared/service/network.service";
import {MyToastService} from "../../core/shared/service/my-toast.service";
import {ProductModel} from "../../core/model/product.model";

@Injectable()
export class BasketFacade {

  productCount: number;
  basketProducts: BasketItem[] = [];

  constructor(private productService: ProductService,
              private basketService: BasketService,
              private orderService: OrderService,
              private logger: LoggingService,
              private network: NetworkService,
              private toastCtrl: MyToastService,
              private events: Events) {

    this.events.subscribe(EventChannels.BASKET_ITEM_REMOVED, product => {
      var index = this.basketProducts.map(value => value.product.id).indexOf(product.id, 0);
      if (index > -1) {
        this.basketProducts.splice(index, 1);
        this.updateBasketProductCount();
      } else {
        this.logger.log(LogLevel.ERROR, "Kon product niet vinden en verwijderen uit het winkelmandje", product);
      }
    });

    this.events.subscribe(EventChannels.BASKET_ITEM_ADDED, (basketItem: BasketItem) => {
      this.basketProducts.push(basketItem);
      this.updateBasketProductCount();
    });

    this.events.subscribe(EventChannels.BASKET_ITEM_UPDATED, (basketItem: BasketItem) => {
      var filteredItems = this.basketProducts.filter(value => value.product.id === basketItem.product.id);
      if (filteredItems.length > 0) {
        var index = this.basketProducts.map(value => value).indexOf(filteredItems[0], 0);

        if (index > -1) {
          this.basketProducts[index] = basketItem;
          this.updateBasketProductCount();
          this.events.publish(EventChannels.BASKET_VISUAL_COUNT, this.basketProducts[index]);
        } else {
          this.logger.log(LogLevel.ERROR, "Kon winkelwagen product niet updaten", basketItem);
        }
      }
    });

    this.events.subscribe(EventChannels.BASKET_LOADED, (basketItems: BasketItem[]) => {
      console.log("Basket loaded: ", basketItems);
      this.basketProducts = basketItems;
      this.updateBasketProductCount();
    });
  }

  getTotalPrice() {
    let totalPrice = 0;
    this.basketProducts.forEach(value => {
      totalPrice = totalPrice + value.getTotalPrice();
    })
    return Math.round(totalPrice*100)/100;
  }

  addToBasket(product: ProductModel) {
    this.basketService.addToBasket(product);
  }

  removeFromBasket(basketItem: BasketItem) {
    this.basketService.removeFromBasket(basketItem);
  }

  removeProductFromBasket(product: ProductModel) {
    this.basketService.removeProductFromBasket(product);
  }

  loadBasket() {
    this.basketService.loadBasket();
  }

  updateBasketProductCount() {
    let count = 0;
    for (let i = 0; i < this.basketProducts.length; i++) {
      count = count + this.basketProducts[i].amount;
    }
    this.productCount = count;
  }

  placeOrder(order: OrderModel) {
    //TODO SET ON IF IT WORKS
    //  if (this.network.hasConnection()) {
    this.orderService.placeOrder(order)
      .then((isPlaced: boolean) => {
        if (isPlaced) {
          //TODO SHOW TOAST
          this.basketService.loadBasket();
          this.orderService.loadOrders();
        } else {
          this.toastCtrl.showErrorToast("Uw order kon niet geplaatst worden, probeer het binnen 5 minuten nog eens. Sorry voor het ongemak.", 5000);
        }
      });
    // } else {
    //   this.logger.log(LogLevel.WARN, "Geen connectie met het internet om order te plaatsen", order);
    // }
  }
}
