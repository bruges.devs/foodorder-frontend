import {Injectable} from "@angular/core";
import {ProductService} from "../../core/service/product.service";
import {BasketService} from "../../core/service/basket.service";
import {LogLevel} from "../../core/shared/constants/loglevel";
import {LoggingService} from "../../core/shared/service/logging.service";
import {Events} from "ionic-angular";
import {EventChannels} from "../../core/constants/event-channels";
import {NetworkService} from "../../core/shared/service/network.service";
import {MyToastService} from "../../core/shared/service/my-toast.service";
import {CategoryService} from "../../core/service/category.service";
import {CategoryModel} from "../../core/model/category.model";
import {ProductModel} from "../../core/model/product.model";
import {ShopModel} from "../../core/model/shop.model";

@Injectable()
export class ProductFacade {

  products: ProductModel[] = [];
  filteredProducts: ProductModel[] = [];
  categories: CategoryModel[] = [];

  constructor(private productService: ProductService,
              private basketService: BasketService,
              private events: Events,
              private toastCtrl: MyToastService,
              private categoryService: CategoryService,
              private network: NetworkService,
              private logger: LoggingService) {
    this.events.subscribe(EventChannels.CATEGORIES_LOADED, (categories: CategoryModel[]) => {
      this.categories = categories;
    });
  }

  initialProductsLoad(): Promise<boolean> {
    this.logger.log(LogLevel.DEBUG, "Initial products load: ", true);
    return new Promise<boolean>((resolve, reject) => {
      return this.productService.loadProducts().then(loadedProducts => {
        this.products = loadedProducts;
        this.filterProducts();
        this.productService.saveProducts(loadedProducts)
          .then(isSaved => {
            if (isSaved) {
              this.basketService.loadBasket();
            } else {
              this.logger.log(LogLevel.ERROR, "Producten konden niet opgeslagen worden: ", isSaved);
              this.products = [];
            }
            resolve(true);
          });
      }).catch(reason => {
        this.logger.log(LogLevel.ERROR, "Error in initialProductsLoad()", reason);
        this.events.publish(EventChannels.PRODUCTS_LOADED_FAILED);
        this.toastCtrl.showToast("De producten konden niet opgehaald worden.", 2000);
      });
    });
  }

  loadProducts() {
    if (this.network.hasConnection()) {
      this.productService.loadProducts().then(loadedProducts => {
        this.products = loadedProducts;
        this.filterProducts();
        this.saveProducts(this.products);
        this.events.publish(EventChannels.PRODUCTS_LOADED);
      }).catch(reason => {
        this.logger.log(LogLevel.ERROR, "Error while loading products", reason);
        this.events.publish(EventChannels.PRODUCTS_LOADED_FAILED);
      });
    }
  }

  saveProducts(products: ProductModel[]) {
    this.productService.saveProducts(products);
  }

  addToBasket(product: ProductModel) {
    this.basketService.addToBasket(product);
  }

  emptyBasket(product: ProductModel) {
    this.basketService.removeProductFromBasket(product);
  }

  loadCategories(): void {
    this.logger.log(LogLevel.DEBUG, "Load categories from DB", null);
    if (this.network) {
      return this.categoryService.loadCategoriesFromApi();
    } else {
      return this.categoryService.loadCategoriesFromDB();
    }
  }

  filterOnCategory(category: CategoryModel) {
    if (!category.isSelectedAsFilter) {
      this.categories.forEach(value => value.isSelectedAsFilter = false);
      category.isSelectedAsFilter = true;
    } else {
      this.categories.forEach(value => value.isSelectedAsFilter = false);
    }
    this.filterProducts();
    this.events.publish(EventChannels.CATEGORIES_FILTER);
  }

  hasSelectedCategory(): boolean {
    return this.categories.filter(value => value.isSelectedAsFilter).length > 0;
  }

  private filterProducts() {
    let categories = this.categories.filter(value => value.isSelectedAsFilter);
    if (categories.length > 0) {
      this.filteredProducts = this.products.filter(value => value.category && value.category.id === categories[0].id);
    } else {
      this.filteredProducts = this.products;
    }
  }

  loadShop(shop: ShopModel) {
    this.events.publish(EventChannels.SHOP_LOADED, shop);
  }
}
