import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';

import {BasketPage} from '../basket/basket';
import {OrdersPage} from '../orders/orders';
import {ProductPage} from '../product/product';
import {Events, NavController, NavParams} from "ionic-angular";
import {EventChannels} from "../../core/constants/event-channels";
import {BasketService} from "../../core/service/basket.service";
import {ShopModel} from "../../core/model/shop.model";
import {SettingsPage} from "../settings/settings";
import {ShopsFavoritePage} from "../shops-favorite/shops-favorite";
import {MyToastService} from "../../core/shared/service/my-toast.service";
import {Tab} from "ionic-angular/navigation/nav-interfaces";
import {BasketItem} from "../../core/model/basket-item";
import {LoggingService} from "../../core/shared/service/logging.service";
import {LogLevel} from "../../core/shared/constants/loglevel";
import {ProductFacade} from "../facade/product.facade";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage implements OnInit, OnDestroy {

  tab1Root = ProductPage;
  tab2Root = BasketPage;
  tab3Root = OrdersPage;
  tab4Root = SettingsPage;

  basketItemCount = 0;
  selectedTab: number = 0;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private events: Events,
              private changeRef: ChangeDetectorRef,
              private toastCtrl: MyToastService,
              private logger: LoggingService,
              private productFacade: ProductFacade,
              private basketService: BasketService) {
    let shop = new ShopModel(JSON.parse(window.localStorage.getItem('shop')));
    //TODO RELOAD EVERYTHING WHEN NEW SHOP IS CHOSEN
  }

  ngOnInit(): void {
    this.events.subscribe(EventChannels.BASKET_UPDATED, () => {
      this.basketService.updateBasketCount();
    });

    this.events.subscribe(EventChannels.BASKET_LOADED, (basketItems: BasketItem[]) => {
      this.logger.log(LogLevel.DEBUG, "Received event in tabs.ts: BASKET_LOADED", basketItems);
      let count = 0;
      for (let i = 0; i < basketItems.length; i++) {
        count =count + basketItems[i].amount;
      }
      this.basketItemCount = count;
    });

    this.events.subscribe(EventChannels.BASKET_COUNT_UPDATED, (basketCount:number) => {
      this.logger.log(LogLevel.DEBUG, "Received event in tabs.ts: BASKET_COUNT_UPDATED", basketCount);
      this.basketItemCount = basketCount;
      if (this.selectedTab != 1) {
        this.toastCtrl.showToast(this.basketItemCount + " " + (this.basketItemCount === 1 ? "product" : "producten") + " in winkelmandje", 1500);
      }
    });

    this.events.subscribe(EventChannels.SETTINGS_SHOW, () => {
      this.navCtrl.push(ShopsFavoritePage, {firstShopChoice: false});
    });

    this.productFacade.initialProductsLoad()
      .then((areProductsLoaded: boolean) => this.productFacade.loadCategories())
  }

  ngOnDestroy(): void {
    console.log(this.events);
    this.events.unsubscribe(EventChannels.CATEGORIES_FILTER);
    this.events.unsubscribe(EventChannels.CATEGORIES_LOADED);
    this.events.unsubscribe(EventChannels.PRODUCTS_LOADED);
    this.events.unsubscribe(EventChannels.PRODUCTS_LOADED_FAILED);
    this.events.unsubscribe(EventChannels.BASKET_COUNT_UPDATED);
    this.events.unsubscribe(EventChannels.BASKET_LOADED);
    this.events.unsubscribe(EventChannels.BASKET_UPDATED);
    this.events.unsubscribe(EventChannels.ORDERS_LOADED);
    this.events.unsubscribe(EventChannels.SETTINGS_SHOW);
    this.events.unsubscribe(EventChannels.SHOP_LOADED);
  }

  tabSelected(tab: Tab) {
    this.selectedTab = tab.index;
  }
}
