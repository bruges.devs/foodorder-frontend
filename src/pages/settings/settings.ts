import {Component} from '@angular/core';
import {Events, IonicPage, NavController, NavParams} from 'ionic-angular';
import {ShopModel} from "../../core/model/shop.model";
import {ShopsFavoritePage} from "../shops-favorite/shops-favorite";
import {EventChannels} from "../../core/constants/event-channels";

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  shop: ShopModel = new ShopModel();

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {
    this.shop = new ShopModel(JSON.parse(window.localStorage.getItem("shop")));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  showAllShopsPage() {
    this.events.publish(EventChannels.SETTINGS_SHOW);
  }
}
