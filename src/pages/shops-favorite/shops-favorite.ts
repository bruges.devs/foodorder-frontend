import {Component} from '@angular/core';
import {App, IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {ShopsFacade} from "../facade/shops.facade";
import {ShopModel} from "../../core/model/shop.model";
import {ShopsPage} from "../shops/shops";
import {TabsPage} from "../tabs/tabs";
import {ProductFacade} from "../facade/product.facade";
import {LoggingService} from "../../core/shared/service/logging.service";

/**
 * Generated class for the ShopsFavoritePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shops-favorite',
  templateUrl: 'shops-favorite.html',
})
export class ShopsFavoritePage {

  title = "Favoriete winkels";
  initialStartup = true;

  constructor(private navCtrl: NavController,
              private appCtrl: App,
              private viewCtrl: ViewController,
              private productFacade: ProductFacade,
              private navParams: NavParams,
              private logger: LoggingService,
              private facade: ShopsFacade) {
    this.initialStartup = this.navParams.get('firstShopChoice') ? false : true;
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter ShopsFavoritePage');
    this.facade.showFavoriteShops(true);
  }

  toggleFavorite(shop: ShopModel) {
    shop.isFavorite = !shop.isFavorite;
    this.facade.showFavoriteShops(true);
  }

  showTabsForShop(shop: ShopModel) {
    window.localStorage.setItem('shop', JSON.stringify(shop));
    if (this.initialStartup) {
      this.navCtrl.setRoot(TabsPage);
    } else {
      this.navCtrl.pop();
    }
  }

  showAddShopsPage() {
    this.navCtrl.push(ShopsPage);
  }
}
