import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShopsFavoritePage } from './shops-favorite';

@NgModule({
  declarations: [
    ShopsFavoritePage,
  ],
  imports: [
    IonicPageModule.forChild(ShopsFavoritePage),
  ],
})
export class ShopsFavoritePageModule {}
