import {Component, OnDestroy, OnInit} from '@angular/core';
import {Events, NavController} from 'ionic-angular';
import {ShopModel} from "../../core/model/shop.model";
import {EventChannels} from "../../core/constants/event-channels";
import {ShopsFacade} from "../facade/shops.facade";

@Component({
  selector: 'page-home',
  templateUrl: 'product.html'
})
export class ProductPage implements OnInit, OnDestroy{

  protected shop: ShopModel= new ShopModel();

  constructor(public navCtrl: NavController, private events:Events,
              private facade: ShopsFacade) {
    this.events.subscribe(EventChannels.SHOP_LOADED, (shop: ShopModel)=>{
      this.shop = shop;
    });
  }

  ngOnDestroy(): void {
  }

  ngOnInit(): void {
  }

}
