import {Component, OnInit} from '@angular/core';
import {Events, Refresher} from 'ionic-angular';
import {OrderFacade} from "../facade/order.facade";
import {EventChannels} from "../../core/constants/event-channels";
import {Config} from "../../config/config";
import {OrderModel} from "../../core/model/order.model";

@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html'
})
export class OrdersPage implements OnInit {

  refresher: Refresher;


  constructor(protected facade: OrderFacade,
              private events: Events) {
    this.events.subscribe(EventChannels.ORDERS_LOADED, () => this.refreshEnded());
  }

  ngOnInit(): void {
    this.facade.loadOrdersFromDatabase();
  }

  doRefresh(refresher: Refresher) {
    this.refresher = refresher;
    this.facade.loadOrdersFromDatabase();
  }

  private refreshEnded() {
    if (this.refresher) {
      this.refresher.complete();
    }
  }

  changeStatus(order: OrderModel) {
    if (Config.IS_IN_DEBUG) {
      if (order.status.isInProgress()) {
        order.status.approve();
      } else {
        order.status.reject();
      }
    }
  }
}
