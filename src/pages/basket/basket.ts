import {ChangeDetectorRef, Component} from '@angular/core';
import {AlertController, Events, ModalController} from 'ionic-angular';
import {BasketFacade} from "../facade/basket.facade";
import {OrderDialogComponent} from "../../components/order-dialog/order-dialog";
import {OrderModel} from "../../core/model/order.model";
import {LoggingService} from "../../core/shared/service/logging.service";
import {EventChannels} from "../../core/constants/event-channels";
import {BasketItem} from "../../core/model/basket-item";
import {LogLevel} from "../../core/shared/constants/loglevel";
import {MyToastService} from "../../core/shared/service/my-toast.service";
import {ProductModel} from "../../core/model/product.model";

@Component({
  selector: 'basket',
  templateUrl: 'basket.html'
})
export class BasketPage {

  minimumWidthToShowDelete: number = 390;
  showDelete: boolean = true;

  constructor(protected facade: BasketFacade,
              private events: Events,
              private modalCtrl: ModalController,
              private logger: LoggingService,
              private toastCtrl: MyToastService,
              private alertCtrl: AlertController,
              private changeRef: ChangeDetectorRef) {
    this.events.subscribe(EventChannels.BASKET_VISUAL_COUNT, (basketItem: BasketItem) => {
      this.logger.log(LogLevel.FINE, "Try to update visual count of basketItem: ", basketItem);
    });
  }

  ionViewWillEnter() {
    this.showDelete = window.screen.width >= this.minimumWidthToShowDelete;
    this.facade.loadBasket();
  }

  addToBasket(product: ProductModel) {
    this.facade.addToBasket(product);
  }

  removeFromBasket(basketItem: BasketItem) {
    this.facade.removeFromBasket(basketItem);
  }

  removeProductFromBasket(product: ProductModel) {
    let alert = this.alertCtrl.create({
      title: "Verwijderen uit winkelmandje",
      message: "U staat op het punt het volgend product te verwijderen uit het winkelmandje: " + product.name,
      buttons: [{
        text: "Annuleren",
        role: 'cancel'
      },
        {
          text: "Verwijderen",
          handler: () => {
            this.facade.removeProductFromBasket(product);
          }
        }
      ]
    });

    alert.present();
  }

  orderProducts() {
    if (this.facade.basketProducts.length > 0) {
      let order: OrderModel = new OrderModel();
      order.orderItems = this.facade.basketProducts;

      const orderModal = this.modalCtrl.create(OrderDialogComponent, {"order": order});
      orderModal.present();
    } else {
      this.toastCtrl.showToast("Geen producten in het winkelmandje", 1500);
    }
  }
}
