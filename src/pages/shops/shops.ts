///<reference path="../facade/shops.facade.ts"/>
import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ShopsFacade} from "../facade/shops.facade";
import {ShopModel} from "../../core/model/shop.model";
import {MyToastService} from "../../core/shared/service/my-toast.service";

/**
 * Generated class for the ShopsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shops',
  templateUrl: 'shops.html',
})
export class ShopsPage {

  searchString: string = '';
  title = "Winkels toevoegen";
  canAddShops = false;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private toastCtrl: MyToastService,
              private facade: ShopsFacade) {
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter ShopsPage');
    this.facade.showFavoriteShops(false);
  }

  toggleFavorite(shop: ShopModel) {
    shop.isFavorite = !shop.isFavorite;
    this.facade.filterShops(this.searchString);
    console.log(this.facade.filteredShops);
    this.toastCtrl.showToast(shop.name + " toegevoegd aan favorieten",2000);
  }

  onCancel($event: UIEvent) {
    this.facade.noFilter();
  }

  onSearchInput($event: UIEvent) {
    this.facade.filterShops(this.searchString);
  }
}
